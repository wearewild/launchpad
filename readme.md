# Information

Starting pad aka Boilerplate for starting new projects at WILD.

# Installation

Simply install via

    npm install

and you're good to go.

# Usage

Always work with the files in the *src* directory. Hit

    gulp watch

while working to automatically compile and optimize files. Then reference files in the *dist* directory in your html.
You can also run it manually without watching by calling

    gulp

LiveReload is built in. Just use the [Chrome Extension](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei) or place it [manually](http://feedback.livereload.com/knowledgebase/articles/86180-how-do-i-add-the-script-tag-manually-).

Bower scripts are installed to src/scripts/bower and thus automatically handled.

# How it works

When run via gulp, first the dist directories are cleaned, then the individual tasks run.
Sass files are compiled, then autoprefixed, minified and saved to *dist/styles/*.min.css*
Scripts run through jshint, are then concated to *dist/main.js*, uglified and then renamed to *main.min.js*
Images are minified via imagemin on optimization level 3, optimized images are cached so they are not running again.
