/*
 *  TODO: maybe add https://github.com/sindresorhus/gulp-size and optionally browsersync
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    autopolyfiller = require('gulp-autopolyfiller'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    csscomb = require('gulp-csscomb'),
    order = require('gulp-order'),
    merge = require('event-stream').merge,
    livereload = require('gulp-livereload');

var path = require('path');
var notificationImage = path.join(__dirname, "wild.png");

// Styles
gulp.task('styles', function() {
  return gulp.src('src/styles/**/*.scss')
    .pipe(sass({ style: 'expanded', }))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(csscomb())
    .pipe(gulp.dest('dist/styles'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('dist/styles'))
    .pipe(notify({
        message: 'Styles task completed',
        icon: notificationImage,
        title: "WILD launchpad"
    }));
});

// Scripts
gulp.task('scripts', function() {
    // Concat all required js files
    var all = gulp.src('src/scripts/**/*.js')
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'))
        .pipe(concat('main.js'));

    // Generate polyfills for all files
    var polyfills = all
        .pipe(autopolyfiller('polyfills.js'));

    // Merge polyfills and all files streams
    return merge(polyfills, all)
        // Order files. NB! polyfills MUST be first
        .pipe(order([
            'polyfills.js',
            'main.js'
        ]))
        // Make single file
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/scripts'))
        // Uglify it
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        // And finally write it to dist
        .pipe(gulp.dest('dist/scripts'))
        .pipe(notify({
            message: 'Scripts task complete',
            icon: notificationImage,
            title: "WILD launchpad"
        }));
});

// Images
gulp.task('images', function() {
  return gulp.src('src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/images'))
    .pipe(notify({
        message: 'Images task complete',
        icon: notificationImage,
        title: "WILD launchpad"
    }));
});

// Clean
gulp.task('clean', function() {
  return gulp.src(['dist/styles', 'dist/scripts', 'dist/images'], {read: false})
    .pipe(clean());
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts', 'images');
});

// Watch
gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('src/styles/**/*.scss', ['styles']);

  // Watch .js files
  gulp.watch('src/scripts/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch('src/images/**/*', ['images']);

  // Create LiveReload server
  var server = livereload();

  // Watch any files in dist/, reload on change
  gulp.watch(['dist/**']).on('change', function(file) {
    server.changed(file.path);
  });

});
