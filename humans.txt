# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Thomas Strobl -- Developer -- @tom2strobl
    Michael Birsak -- Developer -- @drbirsi

# TECHNOLOGY COLOPHON

    CSS3, HTML5, Gulp
